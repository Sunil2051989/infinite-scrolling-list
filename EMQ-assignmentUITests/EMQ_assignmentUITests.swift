//
//  EMQ_assignmentUITests.swift
//  EMQ-assignmentUITests
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import XCTest

class EMQ_assignmentUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        XCUIApplication().launch()
        app = XCUIApplication()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTableExistance() {
        app.launch()
        let table = XCUIApplication().tables
        XCTAssertNotNil(table)
        XCTAssertEqual(table.cells.count, 0, "There should be 0 rows initially")
    }
}
