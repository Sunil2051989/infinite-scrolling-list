//
//  Constants.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import Foundation
import UIKit

// URL Constants
struct kURL {
    static let api = "https://hook.io/syshen/infinite-list?"
}

// Other Constants
struct kConstants {
    //Strings
    static let Alert = "Alert"
    static let OK = "Ok"
}

let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil) // Storyboard constant
let number = 20 // Constant for how much data will return from API call
let estimatedRowHeight = CGFloat(90.0) // Constant for UITableView estimatedRowHeight
