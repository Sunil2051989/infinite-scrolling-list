//
//  Utilities.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import UIKit
import SystemConfiguration

class Utilities: NSObject {
    
    /* format date string(yyyy-MM-dd'T'HH:mm:ss.SSSZ)  to (yyyy-MM-dd HH:mm)
     * @param dateString: given date as string
     * @return  formatted date string object
     */
    static func formatDateString(_ dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        if dateFormatter.date(from: dateString) != nil {
            // Correct date format
            let date = dateFormatter.date(from: dateString)!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" ; //"dd-MM-yyyy HH:mm"
            dateFormatter.timeZone = TimeZone.current
            let formatedString = dateFormatter.string(from: date)
            
            return formatedString
        } else {
            // Invalid format then return string as it is.
            return dateString
        }
    }
    
    
    /* Whether the network is currently reachable.
     * @param NA
     * @return Yes/No
     */
    static func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
