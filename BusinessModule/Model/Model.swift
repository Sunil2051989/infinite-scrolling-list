//
//  model.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import Foundation

struct Model: Codable {
    let id: Int
    let created: String
    let source: Source
    let destination: Destination
}

struct Source: Codable {
    let sender: String
    let note: String
}

struct Destination: Codable {
    let recipient: String
    let amount: Int
    let currency: String
}
