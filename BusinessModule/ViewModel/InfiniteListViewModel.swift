//
//  DataViewModel.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import Foundation
import UIKit

class InfiniteListViewModel {
    
    let apiService: APIServiceProtocol
    
    private var models: [Model] = [Model]()

    private var cellViewModels: [DataCellViewModel] = [DataCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // MARK:- Init 
    init( apiService: APIServiceProtocol = APIService()) {
        self.apiService = apiService
    }
    
    /* Fetch the data list from API by given page index and number
     * @param pageIndex: index of each page
     * @param num: Number of items for how much data will return from API call
     */
    func initFetch(_ pageIndex: Int, _ num: Int) {
        self.isLoading = true
        apiService.fetchData(pageIndex, num) { [weak self] (success, models, error) in
            self?.isLoading = false
            if let error = error {
                self?.alertMessage = error.rawValue
            } else {
                self?.processFetchedData(models)
            }
        }
    }
    
    /* Getting cellViewModel by given index
     * @param indexPath: tableview cell index
     * @return DataCellViewModel
     */
    func getCellViewModel( at indexPath: IndexPath ) -> DataCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    /* Removing all objects from cellViewModels
     * This will needed while pull to refresh data.
     */
    func removeCellViewModels() {
        cellViewModels.removeAll()
    }
    
    // MARK:- Private methods
    /* Creating cellViewModels array by wrapping cells contents
     * @param model: single data model
     */
    private func createCellViewModel(_ model: Model ) -> DataCellViewModel {
        //Wrap a CellViewModel
        let id = String(format: "%d", model.id)
        let amount = String(format: "%@ %d", model.destination.currency, model.destination.amount)
        let date = Utilities.formatDateString(model.created)
        
        // Wrap all contents
        return DataCellViewModel( id: id, sender: model.source.sender, created: date, recipient: model.destination.recipient, amount: amount, note: model.source.note)

    }
    
    /* Processing fetched data and adding to cellViewModels data source array
     * @param models: Array, list of data model
     */
    private func processFetchedData(_ models: [Model] ) {
        var vms = [DataCellViewModel]()
        
        // Adding previous data (on load more)
        if !self.cellViewModels.isEmpty {
            vms = self.cellViewModels
        }
        
        // Append current data
        for model in models {
            vms.append( createCellViewModel(model))
        }
        self.cellViewModels = vms
    }
}

// MARK:- Data Cell View Model
struct DataCellViewModel {
    let id: String
    let sender: String
    let created: String
    let recipient: String
    let amount: String
    let note: String
}
