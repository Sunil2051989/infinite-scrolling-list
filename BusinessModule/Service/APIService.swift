//
//  APIService.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import Foundation



enum APIError: String, Error {
    case noNetwork = "No Network"
    case noData = "No data"
    case unknownError = "Unknown Error"
    case permissionDenied = "You don't have permission"
}

protocol APIServiceProtocol {
    func fetchData(_ startIndex: Int, _ num: Int, complete: @escaping ( _ success: Bool, _ models: [Model], _ error: APIError? )->() )
}

class APIService: APIServiceProtocol {
    
    /* Fetching data through API
     * @param startIndex: data will return from which index
     * @param num: how much datas will return in a api call
     * @return success: yes/No
     * @return models: response array list
     * @return error: error if any
     */
    func fetchData(_ startIndex: Int, _ num: Int, complete: @escaping ( _ success: Bool, _ models: [Model], _ error: APIError? )->() ) {
        // set up URLRequest with URL
        let urlString = String(format: kURL.api + "startIndex=%d&num=%d",startIndex, num)
        let url = URL(string: urlString)
        
        //Make request
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            // handle response to request
            // check for error
            guard error == nil else {
                if Utilities.isInternetAvailable() {
                    complete(false, [], APIError.unknownError)
                }else {
                    complete(false, [], APIError.noNetwork)
                }
                return
            }
            
            // make sure we got data in the response
            guard data != nil else {
                complete(false, [], APIError.noData)
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let models = try! decoder.decode(Array<Model>.self, from: data!)
                complete( true, models, nil )
            }
        }
        task.resume()
    }
}


