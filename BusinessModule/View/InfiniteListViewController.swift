//
//  ViewController.swift
//  EMQ-assignment
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import UIKit

class InfiniteListViewController: UIViewController {
    // IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Controls
    private let refreshControl = UIRefreshControl()
    
    // View model instance
    lazy var viewModel: InfiniteListViewModel = {
        return InfiniteListViewModel()
    }()
    
    // Page index to fetch data as per incremental pages
    private var pageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Init the static view
        initView()
        
        // Init view model
        initViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func initView() {
        // UITableView initial properties
        tableView.estimatedRowHeight = estimatedRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Setup refresh controller
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(pullToReffresh), for: .valueChanged)
    }
    
    func initViewModel() {
        
        // Naive binding
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                    self?.refreshControl.endRefreshing()
                }
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                if isLoading {
                    self?.activityIndicator.startAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.tableView.alpha = self?.pageIndex == 0 ? 0.0 : 1.0
                    })
                }else {
                    self?.activityIndicator.stopAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.tableView.alpha = 1.0
                    })
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.refreshControl.endRefreshing()
            }
        }
        
        viewModel.initFetch(pageIndex, number)
    }
    
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: kConstants.Alert, message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: kConstants.OK, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func pullToReffresh() {
        // Fetch data
        DispatchQueue.global(qos: .background).async {
            // Reset page index and CellViewModels data source
            self.pageIndex = 0
            self.viewModel.removeCellViewModels()
            
            // Fetch new data
            self.viewModel.initFetch(self.pageIndex, number)
        }
    }
}

// MARK:-
extension InfiniteListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as? DataListTableViewCell else {
            fatalError("Cell not exists in storyboard")
        }
        
        let cellVM = viewModel.getCellViewModel( at: indexPath )
        
        cell.lblId.text = cellVM.id
        cell.lblSender.text = cellVM.sender
        cell.lblDate.text = cellVM.created
        cell.lblRecipient.text = cellVM.recipient
        cell.lblAmount.text = cellVM.amount
        cell.lblNote.text = cellVM.note
        cell.lblNote.sizeToFit()
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        // Showing detail view controller
        let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        detailsViewController.view.tag = indexPath.row
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let scrollHeight = scrollView.frame.size.height
        let endScrolling = offsetY + scrollHeight
        
        if endScrolling >= scrollView.contentSize.height {
            // Updating page index
            pageIndex = pageIndex + 1

            // Load more function
            viewModel.initFetch(pageIndex, number)
        }
    }
}

// MARK:-
// DataListTableViewCell class implementation
class DataListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblSender: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRecipient: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblNote: UILabel!
}

// DetailsViewController class implementation
class DetailsViewController: UIViewController {
    @IBOutlet weak var lblSelectedIndex: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblSelectedIndex.text = String(format: "%d", view.tag)
    }
}


