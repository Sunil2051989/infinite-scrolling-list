//
//  UtilitiesTest.swift
//  EMQ-assignmentTests
//
//  Created by Sunil Targe on 2018/9/7.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import XCTest

class UtilitiesTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testformatDateString() {
        let strDate = Utilities.formatDateString("2018-11-07T11:45:58.202Z")
        XCTAssertNotNil(strDate)
        
        let strWrongDate = Utilities.formatDateString("wrongDateFormat")
        XCTAssertNotNil(strWrongDate)
        
        let strEmptyDate = Utilities.formatDateString("")
        XCTAssertNotNil(strEmptyDate)
    }
    
    func testConnectivity() {
        if Utilities.isInternetAvailable() {
            XCTAssertTrue(Utilities.isInternetAvailable())
        } else {
            XCTAssertFalse(Utilities.isInternetAvailable())
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
