//
//  APIServiceTest.swift
//  EMQ-assignmentTests
//
//  Created by Sunil Targe on 2018/9/8.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import XCTest

class APIServiceTest: XCTestCase {
        
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAPIResponse(){
        // Define an expectation
        let expectation = self.expectation(description: "API call and runs the callback closure")
        
        // Making asynchronous request
        let url = URL(string: "https://hook.io/syshen/infinite-list?startIndex=0&num=20")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            // expectation fulfilled
            expectation.fulfill()
            
            // check for error
            if error != nil {
                XCTFail("error occured: \(String(describing: error))")
            }
            
            // check for data in the response
            if data != nil {
                XCTAssertNotNil(data, "We got data in API response")
                XCTAssertNotNil(response)
            } else {
                XCTAssertNil(data)
            }
        }
        task.resume()
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
