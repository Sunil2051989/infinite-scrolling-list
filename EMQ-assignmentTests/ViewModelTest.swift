//
//  ViewModelTest.swift
//  EMQ-assignmentTests
//
//  Created by Sunil Targe on 2018/9/8.
//  Copyright © 2018 Sunil Targe. All rights reserved.
//

import XCTest

class ViewModelTest: XCTestCase {
    
    var viewModel: InfiniteListViewModel!
    var mockAPIService: MockAPIService!
    
    override func setUp() {
        super.setUp()
        mockAPIService = MockAPIService()
        viewModel = InfiniteListViewModel(apiService: mockAPIService)
    }
    
    override func tearDown() {
        super.tearDown()
        viewModel = nil
        mockAPIService = nil
    }
    
    func testFetchInfiniteList() {
        // Given
        mockAPIService.models = [Model]()
        
        // When
        viewModel.initFetch(0, number)
        
        // Assert
        XCTAssert(mockAPIService!.isFetchInfiniteListCalled)
    }
    
    func testFetchInfiniteListFail() {
        
        // Given a failed fetch with a certain failure
        let error = APIError.permissionDenied
        
        // When
        viewModel.initFetch(0, number)
        
        mockAPIService.fetchFail(error: error )
        
        // Sut should display predefined error message
        XCTAssertEqual(viewModel.alertMessage, error.rawValue )
    }
    
    func testLoadingWhenFetching() {
        
        //Given
        var loadingStatus = false
        let expectation = XCTestExpectation(description: "Loading status updated")
        viewModel.updateLoadingStatus = { [weak viewModel] in
            loadingStatus = viewModel!.isLoading
            expectation.fulfill()
        }
        
        //when fetching
        viewModel.initFetch(0, number)
        
        // Assert
        XCTAssertTrue( loadingStatus )
        
        // When finished fetching
        mockAPIService!.fetchSuccess()
        XCTAssertFalse( loadingStatus )
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}

class MockAPIService : APIServiceProtocol {
    
    var isFetchInfiniteListCalled = false
    
    var models: [Model] = [Model]()
    var completeClosure: ((Bool, [Model], APIError?) -> ())!
    
    func fetchData(_ startIndex: Int, _ num: Int, complete: @escaping (Bool, [Model], APIError?) -> ()) {
        isFetchInfiniteListCalled = true
        completeClosure = complete
    }
    
    func fetchSuccess() {
        completeClosure( true, models, nil )
    }
    
    func fetchFail(error: APIError?) {
        completeClosure( false, models, error )
    }
}
