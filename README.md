# Infinite-Scrolling-List

### Author
Suni Targe

### Description
• Retrieving data from below api via setting different parameter for startIndex and
num.
https://hook.io/syshen/infinite-list?startIndex={index}&num={number}
startIndex - means data will return from which index
num - means how many datas will return in a api call.
• Tapping cell will navigate user to a empty page with background color “#0383CC” and
display selected index on this page.
• Cell height should set to dynamic to display all information.

### Architecture
I am using MVVM architecture for this project and adding closures to perform binding between View and ViewModel.

### Language 
Swift 4.2

